# Translation of kcmtelepathyaccounts_plugin_salut.po to Catalan (Valencian)
# Copyright (C) 2012-2014 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2012, 2014.
msgid ""
msgstr ""
"Project-Id-Version: ktp-accounts-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2014-02-15 18:09+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#. i18n: ectx: property (title), widget (QGroupBox, additionalGroupBox)
#: salut-advanced-options-widget.ui:17
#, kde-format
msgid "Additional Information"
msgstr "Informació addicional"

#. i18n: ectx: property (text), widget (QLabel, pubnameLabel)
#: salut-advanced-options-widget.ui:26
#, kde-format
msgid "Published name:"
msgstr "Nom publicat:"

#. i18n: ectx: property (text), widget (QLabel, emailLabel)
#: salut-advanced-options-widget.ui:40
#, kde-format
msgid "Email:"
msgstr "Correu electrònic:"

#. i18n: ectx: property (text), widget (QLabel, jidLabel)
#: salut-advanced-options-widget.ui:54
#, kde-format
msgid "Jabber ID:"
msgstr "ID del Jabber:"

#. i18n: ectx: property (windowTitle), widget (QWidget, SalutMainOptionsWidget)
#: salut-main-options-widget.ui:14
#, kde-format
msgid "Account Preferences"
msgstr "Preferències del compte"

#. i18n: ectx: property (text), widget (QLabel, firstnameLabel)
#: salut-main-options-widget.ui:25
#, kde-format
msgid "First name:"
msgstr "Nom:"

#. i18n: ectx: property (text), widget (QLabel, lastnameLabel)
#: salut-main-options-widget.ui:38
#, kde-format
msgid "Last name:"
msgstr "Cognoms:"

#. i18n: ectx: property (text), widget (QLabel, nicknameLabel)
#: salut-main-options-widget.ui:51
#, kde-format
msgid "Nickname:"
msgstr "Sobrenom:"
