# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Xosé <xosecalvo@gmail.com>, 2009.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2012, 2013, 2017.
# Miguel Branco <mgl.branco@gmail.com>, 2013.
# Marce Villarino <mvillarino@kde-espana.org>, 2013, 2014.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2019, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:11+0200\n"
"PO-Revision-Date: 2023-09-03 11:54+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Adrián Chaves Fernández"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "adrian@chaves.io"

#: account-identity-dialog.cpp:42
#, kde-format
msgid "Edit Account Identity"
msgstr "Editar a identidade da conta"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: account-identity-dialog.ui:40
#, kde-format
msgid "Nickname:"
msgstr "Alcume:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: account-identity-dialog.ui:50
#, kde-format
msgid "Account:"
msgstr "Conta:"

#. i18n: ectx: property (text), widget (AvatarButton, accountAvatar)
#: account-identity-dialog.ui:87
#, kde-format
msgid "..."
msgstr "…"

#: accounts-list-delegate.cpp:70
#, kde-format
msgid "Change account icon"
msgstr "Cambiar a icona da conta"

#: accounts-list-delegate.cpp:71
#, kde-format
msgid ""
"This button allows you to change the icon for your account.<br />This icon "
"is just used locally on your computer, your contacts will not be able to see "
"it."
msgstr ""
"Este botón permite cambiar a icona da súa conta.<br /> A icona utilízase só "
"a nivel local, no seu computador; os seus contactos non a verán."

#: accounts-list-delegate.cpp:79
#, kde-format
msgid "Change account display name"
msgstr "Cambiar o nome para amosar da conta"

#: accounts-list-delegate.cpp:80
#, kde-format
msgid ""
"This button allows you to change the display name for your account.<br />The "
"display name is an alias for your account and is just used locally on your "
"computer, your contacts will not be able to see it."
msgstr ""
"Este botón permite cambiar o nome para amosar da súa conta.<br /> O nome "
"para amosar é un alias da súa conta, e úsase unicamente de maneira local, no "
"seu computador; os seus contactos non poderán velo."

#: accounts-list-delegate.cpp:134
#, kde-format
msgid "Click checkbox to enable"
msgstr "Prema a caixa para activar"

#: accounts-list-delegate.cpp:144
#, kde-format
msgid "Disable account"
msgstr "Desactivar a conta"

#: accounts-list-delegate.cpp:147
#, kde-format
msgid "Enable account"
msgstr "Activar a conta"

#: add-account-assistant.cpp:95 add-account-assistant.cpp:96
#, kde-format
msgid "Step 1: Select an Instant Messaging Network."
msgstr "Primeiro paso: escolla unha rede de mensaxaría instantánea."

#: add-account-assistant.cpp:120
#, kde-format
msgid "Step 2: Fill in the required Parameters."
msgstr "Segundo paso: encha os parámetros requiridos."

#: add-account-assistant.cpp:172
#, kde-format
msgid ""
"To connect to this IM network, you need to install additional plugins. "
"Please install the telepathy-haze and telepathy-gabble packages using your "
"package manager."
msgstr ""
"Para conectarse a esta rede de mensaxaría instantánea, ten que instalar "
"complementos adicionais. Instale os paquetes «telepathy-haze» e «telepathy-"
"gabble» co seu xestor de paquetes."

#: add-account-assistant.cpp:172
#, kde-format
msgid "Missing Telepathy Connection Manager"
msgstr "Falta o xestor de conexións de Telepathy"

#: add-account-assistant.cpp:208 add-account-assistant.cpp:257
#: salut-enabler.cpp:243
#, kde-format
msgid "Failed to create account"
msgstr "Non se puido crear conta"

#: add-account-assistant.cpp:258 salut-enabler.cpp:244
#, kde-format
msgid "Possibly not all required fields are valid"
msgstr "Pode que non todos os campos requiridos sexan correctos."

#: add-account-assistant.cpp:267 salut-enabler.cpp:253
#, kde-format
msgid "Something went wrong with Telepathy"
msgstr "Algo foi mal con Telepathy."

#: avatar-button.cpp:54
#, kde-format
msgid "Load from file..."
msgstr "Cargar dun ficheiro…"

#: avatar-button.cpp:55
#, kde-format
msgid "Clear Avatar"
msgstr "Limpar o avatar"

#: avatar-button.cpp:106
#, kde-format
msgid "Please choose your avatar"
msgstr "Escolla o seu avatar"

#: avatar-button.cpp:155
#, kde-format
msgid "Failed to load avatar."
msgstr "Non se puido cargar o avatar."

#: edit-display-name-button.cpp:61
#, kde-format
msgid "Edit Display Name"
msgstr "Editar o nome para amosar"

#: edit-display-name-button.cpp:70
#, kde-format
msgid "Choose a new display name for your account"
msgstr "Escolla un novo nome para amosar para a súa conta."

#: edit-display-name-button.cpp:77 edit-display-name-button.cpp:87
#, kde-format
msgid ""
"A display name is your local alias for the account, only you will see it."
msgstr ""
"O nome para amosar é o alias local para a súa conta, só o verá vostede."

#: edit-display-name-button.cpp:86
#, kde-format
msgid "New display name"
msgstr "Novo nome para amosar"

#: kcm-telepathy-accounts.cpp:76
#, kde-format
msgid "telepathy_accounts"
msgstr "contas_de_telepathy"

#: kcm-telepathy-accounts.cpp:76
#, kde-format
msgid "Instant Messaging and VOIP Accounts"
msgstr "Contas de mensaxaría instantánea e VOIP"

#: kcm-telepathy-accounts.cpp:78
#, kde-format
msgid "George Goldberg"
msgstr "George Goldberg"

#: kcm-telepathy-accounts.cpp:78 kcm-telepathy-accounts.cpp:79
#: kcm-telepathy-accounts.cpp:80 kcm-telepathy-accounts.cpp:81
#: kcm-telepathy-accounts.cpp:82 kcm-telepathy-accounts.cpp:83
#: kcm-telepathy-accounts.cpp:84
#, kde-format
msgid "Developer"
msgstr "Desenvolvedor"

#: kcm-telepathy-accounts.cpp:79
#, kde-format
msgid "David Edmundson"
msgstr "David Edmundson"

#: kcm-telepathy-accounts.cpp:80
#, kde-format
msgid "Dominik Schmidt"
msgstr "Dominik Schmidt"

#: kcm-telepathy-accounts.cpp:81
#, kde-format
msgid "Thomas Richard"
msgstr "Thomas Richard"

#: kcm-telepathy-accounts.cpp:82
#, kde-format
msgid "Florian Reinhard"
msgstr "Florian Reinhard"

#: kcm-telepathy-accounts.cpp:83
#, kde-format
msgid "Daniele E. Domenichelli"
msgstr "Daniele E. Domenichelli"

#: kcm-telepathy-accounts.cpp:84
#, kde-format
msgid "Martin Klapetek"
msgstr "Martin Klapetek"

#: kcm-telepathy-accounts.cpp:266
#, kde-format
msgid ""
"We have found Kopete logs for this account. Do you want to import the logs "
"to KDE Telepathy?"
msgstr ""
"Atopouse historial de Kopete para esta conta. Quere importar o historial a "
"KDE Telepathy?"

#: kcm-telepathy-accounts.cpp:267
#, kde-format
msgid "Import Logs?"
msgstr "Importar o historial?"

#: kcm-telepathy-accounts.cpp:268
#, kde-format
msgid "Import Logs"
msgstr "Importar o historial"

#: kcm-telepathy-accounts.cpp:269
#, kde-format
msgid "Close"
msgstr "Pechar"

#: kcm-telepathy-accounts.cpp:276
#, kde-format
msgid "Importing logs..."
msgstr "Importando o historial…"

#: kcm-telepathy-accounts.cpp:294 kcm-telepathy-accounts.cpp:303
#, kde-format
msgid "Kopete Logs Import"
msgstr "Importar o historial de Kopete"

#: kcm-telepathy-accounts.cpp:303
#, kde-format
msgid "Kopete logs successfully imported"
msgstr "O historial de Kopete importouse correctamente."

#. i18n: ectx: property (text), widget (KPushButton, removeAccountButton)
#: kcm-telepathy-accounts.cpp:405 kcm-telepathy-accounts.cpp:406
#: main-widget.ui:184
#, kde-format
msgid "Remove Account"
msgstr "Retirar a conta"

#: kcm-telepathy-accounts.cpp:409
#, kde-format
msgid "Remove conversations logs"
msgstr "Retirar o historial de conversas"

#: kcm-telepathy-accounts.cpp:410
#, kde-format
msgid "Are you sure you want to remove the account \"%1\"?"
msgstr "Seguro que quere retirar a conta «%1»?"

#: kcm-telepathy-accounts.cpp:492
#, kde-format
msgid "Install telepathy-salut to enable"
msgstr "Instale «telepathy-salut» para activalo."

#: kcm-telepathy-accounts.cpp:534
#, kde-format
msgid ""
"Something went terribly wrong and the IM system could not be initialized.\n"
"It is likely your system is missing Telepathy Mission Control package.\n"
"Please install it and restart this module."
msgstr ""
"Algo foi moi mal e non se puido inicializar o sistema de mensaxaría "
"instantánea.\n"
"É probábel que no seu sistema falte o paquete de control de misións (Mission "
"Control) de Telepathy.\n"
"Instáleo e reinicie o módulo."

#: KCMTelepathyAccounts/abstract-account-parameters-widget.cpp:59
#, kde-format
msgid "All mandatory fields must be filled"
msgstr "Debe encher todos os campos obrigatorios."

#: KCMTelepathyAccounts/account-edit-widget.cpp:108
#, kde-format
msgid "Connect when wizard is finished"
msgstr "Conectarse ao rematar o asistente."

#: KCMTelepathyAccounts/account-edit-widget.cpp:219
#, kde-format
msgid "Advanced Options"
msgstr "Opcións avanzadas"

#. i18n: ectx: property (text), widget (QPushButton, advancedButton)
#: KCMTelepathyAccounts/account-edit-widget.ui:62
#, kde-format
msgid "Advanced"
msgstr "Avanzado"

#: KCMTelepathyAccounts/dictionary.cpp:33
#, kde-format
msgid "Password"
msgstr "Contrasinal"

#: KCMTelepathyAccounts/dictionary.cpp:34
#, kde-format
msgid "Account"
msgstr "Conta"

#: KCMTelepathyAccounts/dictionary.cpp:35
#, kde-format
msgid "Priority"
msgstr "Prioridade"

#: KCMTelepathyAccounts/dictionary.cpp:36
#, kde-format
msgid "Port"
msgstr "Porto"

#: KCMTelepathyAccounts/dictionary.cpp:37
#, kde-format
msgid "Alias"
msgstr "Alias"

#: KCMTelepathyAccounts/dictionary.cpp:38
#, kde-format
msgid "Register new Account"
msgstr "Rexistrar unha conta nova"

#: KCMTelepathyAccounts/dictionary.cpp:39
#, kde-format
msgid "Server Address"
msgstr "Enderezo do servidor"

#: KCMTelepathyAccounts/dictionary.cpp:40
#, kde-format
msgid "Fallback STUN server address"
msgstr "Enderezo do servidor STUN de reserva"

#: KCMTelepathyAccounts/dictionary.cpp:41
#, kde-format
msgid "Resource"
msgstr "Recurso"

#: KCMTelepathyAccounts/dictionary.cpp:42
#, kde-format
msgid "HTTPS Proxy Port"
msgstr "Porto do mandatario HTTPS"

#: KCMTelepathyAccounts/dictionary.cpp:43
#, kde-format
msgid "Require Encryption"
msgstr "Require cifrado"

#: KCMTelepathyAccounts/dictionary.cpp:44
#, kde-format
msgid "Old-style SSL support"
msgstr "Compatibilidade co antigo SSL"

#: KCMTelepathyAccounts/dictionary.cpp:45
#, kde-format
msgid "Fallback STUN port"
msgstr "Porto do STUN de reserva"

#: KCMTelepathyAccounts/dictionary.cpp:46
#, kde-format
msgid "Fallback Conference Server Address"
msgstr "Enderezo do servidor de conferencias de reserva"

#: KCMTelepathyAccounts/dictionary.cpp:47
#, kde-format
msgid "Low Bandwidth Mode"
msgstr "Modo de pouca largura de banda"

#: KCMTelepathyAccounts/dictionary.cpp:48
#, kde-format
msgid "STUN Server Address"
msgstr "Enderezo do servidor STUN"

#: KCMTelepathyAccounts/dictionary.cpp:49
#, kde-format
msgid "STUN Port"
msgstr "Porto STUN"

#: KCMTelepathyAccounts/dictionary.cpp:50
#, kde-format
msgid "Fallback SOCKS5 Proxy Addresses"
msgstr "Enderezos de reserva do mandatario SOCKS5"

#: KCMTelepathyAccounts/dictionary.cpp:51
#, kde-format
msgid "HTTPS Proxy Server Address"
msgstr "Enderezo do servidos do mandatario HTTPS"

#: KCMTelepathyAccounts/dictionary.cpp:52
#, kde-format
msgid "Ignore SSL Errors"
msgstr "Ignorar os erros de SSL"

#: KCMTelepathyAccounts/dictionary.cpp:53
#, kde-format
msgid "Keepalive Interval"
msgstr "Intervalo de keepalive"

#: KCMTelepathyAccounts/dictionary.cpp:56
#, kde-format
msgid "AOL Instant Messenger"
msgstr "Mensaxaría instantánea AOL"

#: KCMTelepathyAccounts/dictionary.cpp:57
#: KCMTelepathyAccounts/dictionary.cpp:63
#, kde-format
msgid "Skype"
msgstr "Skype"

#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonFacebook)
#: KCMTelepathyAccounts/dictionary.cpp:58
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:124
#, kde-format
msgid "Facebook Chat"
msgstr "Charla de Facebook"

#: KCMTelepathyAccounts/dictionary.cpp:59
#, kde-format
msgid "Gadu-Gadu"
msgstr "Gadu-Gadu"

#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonGTalk)
#: KCMTelepathyAccounts/dictionary.cpp:60
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:100
#, kde-format
msgid "Google Talk"
msgstr "Google Talk"

#: KCMTelepathyAccounts/dictionary.cpp:61
#, kde-format
msgid "Novell Groupwise"
msgstr "Novell Groupwise"

#: KCMTelepathyAccounts/dictionary.cpp:62
#, kde-format
msgid "ICQ"
msgstr "ICQ"

# skip-rule: PT-2011_chat
#: KCMTelepathyAccounts/dictionary.cpp:64
#, kde-format
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonJabber)
#: KCMTelepathyAccounts/dictionary.cpp:65
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:54
#, kde-format
msgid "Jabber/XMPP"
msgstr "Jabber/XMPP"

#: KCMTelepathyAccounts/dictionary.cpp:66
#, kde-format
msgid "Bonjour/Salut"
msgstr "Bonjour/Salut"

#: KCMTelepathyAccounts/dictionary.cpp:67
#, kde-format
msgid "MXit"
msgstr "MXit"

#: KCMTelepathyAccounts/dictionary.cpp:68
#, kde-format
msgid "Windows Live Messenger"
msgstr "Windows Live Messenger"

#: KCMTelepathyAccounts/dictionary.cpp:69
#, kde-format
msgid "MySpaceIM"
msgstr "MySpaceIM"

#: KCMTelepathyAccounts/dictionary.cpp:70
#, kde-format
msgid "Tencent QQ"
msgstr "Tencent QQ"

#: KCMTelepathyAccounts/dictionary.cpp:71
#, kde-format
msgid "IBM Lotus Sametime"
msgstr "IBM Lotus Sametime"

#: KCMTelepathyAccounts/dictionary.cpp:72
#, kde-format
msgid "SILC"
msgstr "SILC"

#: KCMTelepathyAccounts/dictionary.cpp:73
#, kde-format
msgid "Session Initiation Protocol (SIP)"
msgstr "Protocolo de inicio de sesión (SIP)"

#: KCMTelepathyAccounts/dictionary.cpp:74
#, kde-format
msgid "GSM Telephony"
msgstr "Telefonía GSM"

#: KCMTelepathyAccounts/dictionary.cpp:75
#, kde-format
msgid "Trepia"
msgstr "Trepia"

#: KCMTelepathyAccounts/dictionary.cpp:76
#, kde-format
msgid "Yahoo! Messenger"
msgstr "Yahoo! Messenger"

#: KCMTelepathyAccounts/dictionary.cpp:77
#, kde-format
msgid "Yahoo! Messenger Japan"
msgstr "Yahoo! Messenger Xapón"

#: KCMTelepathyAccounts/dictionary.cpp:78
#, kde-format
msgid "Zephyr"
msgstr "Zephyr"

#: KCMTelepathyAccounts/parameter-edit-model.cpp:301
#, kde-format
msgid "Parameter \"<b>%1</b>\" is not valid."
msgstr "O parámetro «<b>%1</b>» non é correcto."

#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonKDETalk)
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:78
#, kde-format
msgid "KDETalk"
msgstr "KDETalk"

#. i18n: ectx: property (text), widget (QCommandLinkButton, buttonOthers)
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:148
#, kde-format
msgid "All"
msgstr "Todos"

#. i18n: ectx: property (description), widget (QCommandLinkButton, buttonOthers)
#: KCMTelepathyAccounts/simple-profile-select-widget.ui:162
#, kde-format
msgid "AOL, Gadu-Gadu, Yahoo and more..."
msgstr "AOL, Gadu-Gadu, Yahoo e máis…"

#: KCMTelepathyAccounts/validated-line-edit.cpp:100
#, kde-format
msgid "This field is required"
msgstr "Este campo é obrigatorio."

#: KCMTelepathyAccounts/validated-line-edit.cpp:121
#, kde-format
msgid "This field should not be empty"
msgstr "Non pode deixar baleiro este campo."

#: KCMTelepathyAccounts/validated-line-edit.cpp:125
#, kde-format
msgid "This field should contain an email address"
msgstr "Este campo debería conter un enderezo de correo electrónico"

#: KCMTelepathyAccounts/validated-line-edit.cpp:129
#, kde-format
msgid "This field should contain a hostname"
msgstr "Este campo debería conter un nome de máquina"

#: KCMTelepathyAccounts/validated-line-edit.cpp:133
#, kde-format
msgid "This field should contain an IP address"
msgstr "Este campo debería conter un enderezo de IP"

#: KCMTelepathyAccounts/validated-line-edit.cpp:178
#, kde-format
msgid "This field is valid"
msgstr "O campo non é correcto."

#. i18n: ectx: property (text), widget (QLabel, salutEnableLabel)
#: main-widget.ui:80
#, kde-format
msgid "Enable local network discovery"
msgstr "Activar o descubrimento de contactos na rede local"

#. i18n: ectx: property (text), widget (QLabel, salutEnableStatusLabel)
#: main-widget.ui:100
#, kde-format
msgid "Connect to people near you"
msgstr "Conéctese coa xente próxima a vostede."

#. i18n: ectx: property (text), widget (KPushButton, addAccountButton)
#: main-widget.ui:151
#, kde-format
msgid "Add Account"
msgstr "Engadir unha conta"

#. i18n: ectx: property (text), widget (KPushButton, editAccountButton)
#: main-widget.ui:161
#, kde-format
msgid "Edit Account"
msgstr "Editar a conta"

#. i18n: ectx: property (toolTip), widget (KPushButton, editAccountIdentityButton)
#: main-widget.ui:171
#, kde-format
msgid "Edit how you appear to others. Requires the account to be online."
msgstr "Edite como lle aparece aos demais. A súa conta ten que estar en liña."

#. i18n: ectx: property (text), widget (KPushButton, editAccountIdentityButton)
#: main-widget.ui:174
#, kde-format
msgid "Edit Identity"
msgstr "Editar a identidade"

#: salut-message-widget.cpp:50
#, kde-format
msgid "Configure manually..."
msgstr "Configurar manualmente…"

#: salut-message-widget.cpp:54
#, kde-format
msgid "Cancel"
msgstr "Cancelar"

#: salut-message-widget.cpp:104
#, kde-format
msgid "You will appear as \"%1\" on your local network."
msgstr "Aparecerá como «%1» na súa rede local."

#~ msgid "Others"
#~ msgstr "Outros"

#~ msgid ""
#~ "The file you have selected does not seem to be an image.\n"
#~ "Please select an image file."
#~ msgstr ""
#~ "O ficheiro seleccionado non parece unha imaxe.\n"
#~ "Escolla un ficheiro de imaxe."

#~ msgid "Online"
#~ msgstr "Conectado"

#~ msgctxt "This is a connection state"
#~ msgid "Connecting"
#~ msgstr "Conectándose"

#~ msgctxt "This is a connection state"
#~ msgid "Disconnected"
#~ msgstr "Desconectado"

#~ msgctxt "This is an unknown connection state"
#~ msgid "Unknown"
#~ msgstr "Descoñecido"

#~ msgctxt "This is a disabled account"
#~ msgid "Disabled"
#~ msgstr "Desactivado"
